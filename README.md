# replay-service

## General information 

## Run 
To run the Replay service, run:

On windows: ```gradle run```
On Mac:  ```./gradlew run```

To format the code: 

On windows ```gradle goJF```
On Mac: ```./gradlew goJF```

To run and format the code:

On windows: ```gradle cleanrun```
On Mac: ```./gradlew cleanrun```
