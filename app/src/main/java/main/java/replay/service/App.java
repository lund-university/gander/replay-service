package main.java.replay.service;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import spark.Request;
import spark.Response;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class App {
    private static final int DEFAULT_PORT = 8007;
    private static String logsFolderPath;
    private Logger logger;

    public static void main(String[] args) {
        int port = args.length > 0 ? parsePortArgument(args[0]) : DEFAULT_PORT;
        port(port);
        Logger logger = setupLogger(DEFAULT_PORT);
        PingService p = new PingService(logger);
        WebsocketServer websocket = new WebsocketServer(new InetSocketAddress( 3004), logger);

        websocket.start();

        //For testing the connection 
        get("/ping", (req, res)-> p.ping(req, res));
        //to set path to the log file
        post("/path", (req, res)->websocket.setPath(req, res));

        get("/getpaths", (req, res)->websocket.sendPaths(req, res));


    }
    public static int parsePortArgument(String portNbr) {
      try {
        return Integer.parseInt(portNbr);
      } catch (NumberFormatException e) {
        System.out.println(
            "Could not resolve the argument to a port number, using the default port number "
                + DEFAULT_PORT);
        return DEFAULT_PORT;
      }
    }
     /**
   * Initializes a Logger object. Creates a new entry in the log-folder.
   *
   * @param port port number the service is active on
   * @return the Logger object
   */
  public static Logger setupLogger(int port) {
    createLogFolder();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
    Logger logger = Logger.getLogger(App.class.getName());
    FileHandler fh;
    try {
      fh =
          new FileHandler(
              logsFolderPath + "/Log_" + dtf.format(LocalDateTime.now()) + "_port" + port + ".log",
              true);
      fh.setFormatter(new SimpleFormatter());
      logger.addHandler(fh);
      // Change the line below for more/less logging info
      logger.setLevel(Level.ALL);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return logger;
  }
  
  /** Creates a folder for the logs, if it doesn't already exist. */
  private static void createLogFolder() {
    logsFolderPath = Paths.get("logs").toAbsolutePath().toString();
    try {
      Files.createDirectories(Paths.get(logsFolderPath));
    } catch (IOException e) {
      System.out.println("Error while creating the logs directory. ");
    }
  }

}
