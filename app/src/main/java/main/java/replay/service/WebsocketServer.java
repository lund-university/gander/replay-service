package main.java.replay.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.JSONArray;


import spark.Request;
import spark.Response;

public class WebsocketServer extends WebSocketServer {
     private final static String CLASS_NAME=WebsocketServer.class.getName();
     private boolean ready;
     private String path;
     private Logger logger;
     

    public WebsocketServer(InetSocketAddress address, Logger logger) {
        super(address); 
        this.ready = false;
        this.logger = logger;
        logger.info("Starting websocket server on address: "+address.toString());
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
       logger.info( "Opened server");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
       logger.entering(CLASS_NAME, "Closing the server now");
    } 
/**
 * Starts sending logs when websocket client is ready
 */
    @Override
    public void onMessage(WebSocket conn, String message) {
        logger.info( "Got the message: "+ message);
        if(ready && message.contains("ready")){
          sendLogs();
        }
    }
    
    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        logger.severe(CLASS_NAME + " exception occured on connection: " + conn.toString()+" with exception: "+ex.getMessage());
    }

    @Override
    public void onStart() {
        logger.entering(CLASS_NAME, "Starting replay service on port " + 8007);
    }
    /*
     * Lists the paths for the different available log files in replay.
     * Should add so0me functionality to connect these with bufferlogs later
     * 
     * When working towards using docker to host the platform this will have to be changed
     * We need to remove all dependencies on local file structures and instead use docker volumes
     */
    public String sendPaths(Request req, Response res){
      JSONObject jsonResponse = new JSONObject();
      try{
        res.status(HttpURLConnection.HTTP_CREATED);

        String userDir = System.getProperty("user.dir"); 
        //int ind = userDir.indexOf("replay-service");
        //String pygmyDir = userDir.substring(0, ind)+File.separatorChar;
        //Path logPath = Paths.get(pygmyDir,"storage-service","app","logs");
        Path logPath = Paths.get(userDir, "logs");
        jsonResponse.put("path",logPath.toString());

        File f = logPath.toFile();
        JSONArray fileArr = new JSONArray();
        for (String file : f.list()){
          System.out.println("File in replayService: "+file);
          if (!file.contains("log")) {
            fileArr.add(file);
          }
        }
        jsonResponse.put("files", fileArr);
      } catch(Exception e){
        jsonResponse.put("status", "ERROR");
        logger.severe("Error while retrieving path list");
      }
      System.out.println("Files sent to client: "+jsonResponse.toString());
      return jsonResponse.toString();
    }
    /*
     * Sets and checks the path for the file we want to replay, sort is not used currently since they are formatted as they're used
     */
    public String setPath(Request req, Response res){
        JSONObject jsonResponse = new JSONObject();
        
        try{
          path=req.body().substring(1, req.body().length()-1);

          res.status(HttpURLConnection.HTTP_CREATED);
          if(checkPath()) {
            
            jsonResponse.put("status", "SUCCESS");
            logger.entering(CLASS_NAME, "We put the path: "+path);
            ready=true;
            //sortJsonFiletoObject(path); //not currently necessary
          }
          else{
              jsonResponse.put("status", "ERROR");
              logger.severe("The path pointed to the wrong file");
          }
        } catch(Exception e){
            jsonResponse.put("status", "ERROR");
            logger.severe("We couldn't enter the path: "+path+" Error: "+e.getMessage());
        }
        return jsonResponse.toString();
    }


   
    /*
     * Checks if a file with the path [path] exists locally.
     */
    public boolean checkPath(){
      try{
          File file = new File(path);           
          return file.exists();
      }
      catch(Exception e){
          logger.severe("Got an exception after trying to find file for path: "+path+" Exception: "+e.getMessage());
      }
      return false;
    }
  
    /*
     * Broadcasts the logs in the file to the websocket client after it's ready. Sorts out the 
     * eyetracker logs which aren't converted by coordinate converter so as to prevent doubles. 
     */
    public void sendLogs() {
        ArrayList<String> logList = sortLogs();
        try {
          for(String s : logList){
            //Try print all broadcasted data to find out if ET is 
            System.out.println(s);
            broadcast(s);
          }
        } catch (Exception e) {
          System.out.println("Problem with broadcasting file\nException: "+e.getMessage());
        }
        broadcast("done");
    }


    /** 
     * Creates a list of the  file which are sorted by timestamps
     * 
     */   
  private ArrayList<String> sortLogs() {
    ArrayList<String> ReqResList= new ArrayList<String>();
    ArrayList<String> worklist= new ArrayList<String>();
    try{
    BufferedReader reader = new BufferedReader(new FileReader(path));
      try {
          String s;
          int eyeTindex=0;
          while ((s=reader.readLine())!=null){
            if(s.contains("TimeRes")&&s.contains("TimeReq")){
              ReqResList.add(s);
             } else if (s.contains("msSinceEpoch")){
              worklist.add(s);
             } else if (s.contains("\"ts\":")){
               eyeTindex++;
               if(eyeTindex==3){
                 worklist.add(s);
                 eyeTindex=0;
               }
             }
          }    
          System.out.println("Curr path:" + path);
          worklist.sort((String s1, String s2)->{
            try{
              JSONObject j1 = (JSONObject) JSONValue.parse(s1);
              JSONObject j2 = (JSONObject) JSONValue.parse(s2);
              long i1;
              long i2;
              if(j1.containsKey("msSinceEpoch")){
                i1=Long.parseLong((String)j1.get("msSinceEpoch"));
              }
              else{
                i1=Long.parseLong((String)j1.get("ts"));
              }
              if(j2.containsKey("msSinceEpoch")){
                i2=Long.parseLong((String)j2.get("msSinceEpoch"));
              }
              else{
                i2=Long.parseLong((String)j2.get("ts"));

              }
              if(i1-i2<0){
                return -1;
              }else if(i1-i2>0){
                return 1;
              }
              return 0;
            }
            catch(NumberFormatException n){
              System.out.println("trouble with sorting " + s1+s2+"\n"+n.getMessage());
            }
            return 0;
          });
          worklist.addAll(ReqResList);
          reader.close();
          return worklist;
      } catch (IOException e) {
        System.out.println("Problem with reading the file");
      }
    }catch(FileNotFoundException f){
      System.out.println("File can't be found");
    }
    return null;
  }

}
